/*
 * test.c
 *
 * Copyright (C) 2018 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Test a database encrypted by sqlite3aes
 *
 * Compile with
 *   gcc -ldl -lpthread test.c -o test
 *
 * Or if you want OpenSSL libcrypto,
 *   gcc -ldl -lpthread -lcrypto test.c -o test
 *
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * Add these includes if you want to use OpenSSL's hash and cipher.
 */
/*
#include <openssl/sha.h>
#include <openssl/aes.h>
*/

/**
 * Get SQLite3 and our encryption codec implementation
 */
#define SQLITE3AES_SALT "MyTestApplication"
#include "sqlite3aes.h"

/**
 * Main function
 */
int main(int argc, char **argv)
{
    char *errstr=NULL, *password="My Very Secret Passphrase";
    sqlite3 *db=NULL;
    sqlite3_stmt *s;
    int rc;

    /* open the database, create a table and put some data in it */
    rc=sqlite3_open("testenc.db", &db);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to open database. rc=%d\n",rc);
        return 1;
    }

    rc=sqlite3_key_v2(db, NULL, password, strlen(password));
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to set up encryption. rc=%d\n",rc);
        return 1;
    }

    rc=sqlite3_exec(db, "CREATE TABLE test (ID VARCHAR NOT NULL UNIQUE PRIMARY KEY,"
        "Name VARCHAR NOT NULL DEFAULT '');", 0, 0, &errstr);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to create table. rc=%d msg=%s\n",rc,errstr);
        return 1;
    }

    rc=sqlite3_exec(db, "INSERT INTO test (ID,Name) VALUES ('dog','Dog')", 0, 0, &errstr);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to insert to table. rc=%d msg=%s\n",rc,errstr);
        return 1;
    }

    rc=sqlite3_exec(db, "INSERT INTO test (ID,Name) VALUES ('cat','Cat')", 0, 0, &errstr);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to insert to table. rc=%d msg=%s\n",rc,errstr);
        return 1;
    }

    sqlite3_close(db);
    db=NULL;
    printf("Records written to table 'test' successfully.\n");

    /* now open database again and try to read records back */
    rc=sqlite3_open("testenc.db", &db);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to open database. rc=%d\n",rc);
        return 1;
    }

    rc=sqlite3_key_v2(db, NULL, password, strlen(password));
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to set up encryption. rc=%d\n",rc);
        return 1;
    }

    rc=sqlite3_prepare_v2(db, "SELECT Name FROM test WHERE ID='dog'", -1, &s, NULL);
    if(rc!=SQLITE_OK) {
        fprintf(stderr, "Unable to select from database. rc=%d\n",rc);
        return 1;
    }
    rc=sqlite3_step(s);
    if(rc!=SQLITE_ROW || strcmp((char*)sqlite3_column_text(s, 0), "Dog")) {
        fprintf(stderr, "Unable to read record from database. rc=%d\n",rc);
        return 1;
    }
    sqlite3_finalize(s);
    sqlite3_close(db);
    printf("Records read back from 'test' successfully.\n");

    return 0;
}

