SQLite3 AES C Header
====================

This is a free, Open Source drop-in replacement for the proprietary SQLite3 SEE. This is a single, portable ANSI C99 header
file without any cryptographic dependencies at all (no OpenSSL needed!).

As taking the minimalistic approach, it does not support entire database encryption / decryption, only encrypted database creation
and operation. Also for reduced code size and simplicity, it only supports SHA-256 hash and AES-256-CBC cipher (both are strong
enough). Therefore you can't open a database encrypted with SEE in sqlite3aes and vice versa, the compatibility is at C API level.

***!!! IMPORTANT NOTE !!!***

Sqlite3 developers are dicks, they have [silently removed the SQLITE_HAS_CODEC API](https://sqlite.org/forum/forumpost/08481b3fac)
without any warnings, notification or even just mentioning this change in the release notes! Therefore this code won't work with
version 3.32 and above. I've included the latest (3.31.1) amalgamation of Sqlite3 that still works.

Usage
-----

First of all, you'll need a standard, public SQLite3 amalgamation, sqlite3.c and sqlite3.h. Then in exactly one of your source
files, do:

```c
#define SQLITE3AES_SALT "SomethingUniqueToYourApplication"
#include "sqlite3aes.h"
```

This will include the implementation, with all of the sqlite3 stuff too (over 7Mbyte). If you prefer OpenSSL's libcrypto over the
well-prooven and super-fast sqlite3aes version, you just have to include the OpenSSL headers first:

```c
#include <openssl/sha.h>
#include <openssl/aes.h>

#define SQLITE3AES_SALT "SomethingUniqueToYourApplication"
#include "sqlite3aes.h"
```

In the source file where you want to open the database, do as you would with SQLite3 SEE:

```c
#include "sqlite3aes.h"

sqlite3* opendb(char *dbfile)
{
    sqlite3 *db;
    char *password = "your very secret password";

    /* open the database */
    sqlite3_open(dbfile, &db);

    /* set up encryption */
    sqlite3_key_v2(db, NULL, password, strlen(password));

    return db;
}
```

That's it, no additional libraries required when linking.

License
-------

Distibuted under the MIT license, you are free to use.

Author
------

bzt (bztsrc@gitlab), AES code originally taken from OpenSSL, but optimized a lot.

